#! /usr/bin/env python

import matplotlib.pyplot as plt 
import numpy as np
import math

t = np.linspace(-4, 4, 400)

#a = np.sin(t)
#b = np.cos(t)
#a = (t+2)**2
#b = t**5 - t**3 + t**2 - t + 1
#c = np.log(t)

c = 0

for i in range(10):
    a = np.sin(t * i)
    #b = i * np.cos(t)
    c = c + a

def logistic(func_range, midpoint, steepness, domain):
    return func_range / (1 + np.exp(-1 * steepness *(domain - midpoint)))

x = np.arange(-4, 4, 0.1)
e = logistic(1, 0.5, 2, x)

#d = 1 / (1 + np.exp(-1 * t))
#d = logistic(2, 1000, 1, t)

d = 1 - (1 / (1 + np.exp(-1 * 2 *(t - 0.5))))

#plt.plot(t, a, t, b, t, c)
#plt.plot(t, c, t, d)
plt.plot(t, d, x, e)
plt.show()