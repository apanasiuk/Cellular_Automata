#! /usr/bin/env python

import matplotlib.pyplot as plt 
import numpy as np
import math

t = np.linspace(-30, 30, 400)
a = 0.5 * t * t * t * t * t - 20 * t * t  * t +  2 * t - 4

def logistic(func_range, midpoint, steepness, domain):
    return func_range / (1 + np.exp(-1 * steepness *(domain - midpoint)))

x = np.arange(-4, 4, 0.1)
e = logistic(1, 0.5, 2, x)
f = logistic(2, 0.5, 4, x)

c = 0
for i in range(4):
    a = np.sin(t * i)
    #b = 0.01 * i * np.cos(t)
    c = c + a

plt.plot(t, c)

plt.show()