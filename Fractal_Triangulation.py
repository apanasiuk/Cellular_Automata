import Rhino.Geometry as rg
import math as math 

def SubdivideTMesh(T):# this function takes a triangle mesh(with one face)and returns a mesh with 4 faces
    stm=rg.Mesh()
    v=T.Vertices
    v3=rg.Point3f(0.5*(v[0].X+v[1].X),0.5*(v[0].Y+v[1].Y),0.5*(v[0].Z+v[1].Z))#for Point3f pointa addition is not defined, instead x,y and z should be added together  
    v4=rg.Point3f(0.5*(v[1].X+v[2].X),0.5*(v[1].Y+v[2].Y),0.5*(v[1].Z+v[2].Z))
    v5=rg.Point3f(0.5*(v[2].X+v[0].X),0.5*(v[2].Y+v[0].Y),0.5*(v[2].Z+v[0].Z))
    stm.Vertices.Add(v[0])
    stm.Vertices.Add(v[1])
    stm.Vertices.Add(v[2])
    stm.Vertices.Add(v3)
    stm.Vertices.Add(v4)
    stm.Vertices.Add(v5)
    stm.Faces.AddFace(0,3,5)
    stm.Faces.AddFace(3,4,5)
    stm.Faces.AddFace(3,1,4)
    stm.Faces.AddFace(2,5,4)
    return stm


def SubdivideAllTriangles(TM):# this function takes a triangle mesh(with several faces) and returns another(one mesh) triangle mesh 
    faceCount=TM.Faces.Count
    STM=rg.Mesh()
    for i in range(0,faceCount):
        face=TM.Faces[i]
        n0=face[0] # this gives the index of face vertex
        n1=face[1]
        n2=face[2]
        STMi=rg.Mesh()
        v=STMi.Vertices
        v.Add(TM.Vertices[n0])
        v.Add(TM.Vertices[n1])
        v.Add(TM.Vertices[n2])
        STMi.Faces.AddFace(0,1,2)
        subdividedTM=SubdivideTMesh(STMi)
        STM.Append(subdividedTM) # append connects all the meshes together and make one
    STM.Vertices.CullUnused
    STM.Vertices.CombineIdentical(False, False)
    return STM
 

nstm=SubdivideTMesh(Tm)#initialize

for i in range(1,n):
    nstm=SubdivideAllTriangles(nstm)
a=nstm