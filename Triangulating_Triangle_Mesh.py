import Rhino.Geometry as rg

import math as math

v=m.Vertices
sp=v[0]
V0=rg.Vector3d(v[0])
V1=rg.Vector3d(v[1])
V2=rg.Vector3d(v[2])
VV=(rg.Vector3d(V1-V0))/n
VH=(rg.Vector3d(V2-V1))/n

pt=[]
TM=rg.Mesh()

for i in range(0,n+1):
    pp=rg.Point3d(sp)
    pp=pp+i*VV# creating parents
    pt.Add(pp)
    TM.Vertices.Add(pp)
    for j in range (0,i):
        pch=pp+VH*(j+1)#creating children
        pt.Add(pch)
        k=len(pt)
        k=k-1
        TM.Vertices.Add(pch)
        TM.Faces.AddFace(k,k-1,k-i-1)
        if j<i-1:
            TM.Faces.AddFace(k,k-i-1,k-i)

a = TM