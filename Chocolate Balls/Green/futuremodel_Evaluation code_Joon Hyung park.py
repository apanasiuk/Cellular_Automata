# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 12:33:26 2018

@author: Park
"""

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from math import pi
from itertools import izip as zip
import matplotlib.patches as mpatches

# make it more readable and more elegantly, aggregated number visualization, showing which policy user chose in every turn, 
#and make checking radar chart by turn available.
#Data import =================================================================================
Av_biomass = np.genfromtxt('D:\\futuremodel final\\average_biomass_results.csv', dtype='float', delimiter=',')
Av_landuse = np.genfromtxt('D:\\futuremodel final\\average_biotic_results.csv', dtype='float', delimiter=',')
Av_temp = np.genfromtxt('D:\\temperature_data.csv', dtype='float', delimiter=',')
Av_co2 = np.genfromtxt('D:\\co2_data_1.csv', dtype='float', delimiter=',')



r3 =list(Av_biomass)
r4 = list(Av_landuse)
r5 = list(Av_temp)
r6 = list(Av_co2)
#data importing from csv file and turn it into list


#============================================================================================

#------------------------Biomass--------------------------------------
# biomass will be evaluated by how much it increase by comparing last tunr
result_biomass = [] 

for i in r3:
    
    if 0.0 <= i <= 1/10:
        result_biomass.append(1)
        
    if 1/10 <= i <= 2/10:
        result_biomass.append(2) 
 
    if 2/10 < i <= 3/10:
        result_biomass.append(3)       

    if 3/10 < i <= 4/10:
        result_biomass.append(4)
        
    if 4/10 < i <= 20/10:
        result_biomass.append(5) 
    
        
#=================Counting grade for overall score    
        #calculating average
bg = sum(result_biomass) / 9

#________________________Temperature------------------------------------

res = [y-x for x, y in zip(r5, r5[1:])] 
#substracting elements in one list
#temperarture increasing and decrease below certain level also earn low score

tem_list = []

for t in res:
    
    if -0.3 <= t < -0.1:
        tem_list.append(5)

    if -0.1 <= t < 0:
        tem_list.append(4)

    if 0 <= t < 0.1:
        tem_list.append(3)

    if 0.1 <= t < 0.2:
        tem_list.append(2)

    if 0.2 <= t < 2 or t <= -0.3:
        tem_list.append(1)

#-----------------co2---------------------------------------------------

co2 = [y-x for x, y in zip(r6, r6[1:])] 

co2_list = []

for c in co2:
    
    if  c < -100:
        co2_list.append(5)

    if -100 <= c < 0:
        co2_list.append(4)

    if 0 <= c < 50:
        co2_list.append(3)

    if 50 <= c < 100:
        co2_list.append(2)
 
    if 100 <= c :
        co2_list.append(1)
    
    if i == 'nan':
        co2_list. append(1)
        


#--------------------------------biotic ---------------------------------
result_biotic = []

for i in r4:
    
    if 0.0 <= i <= 1/5:
        result_biotic.append(1)
        
    if 1/5 <= i <= 2/5:
        result_biotic.append(2)
 
    if 2/5 <= i <= 3/5:
        result_biotic.append(3)       

    if 3/5 <= i <= 4/5:
        result_biotic.append(4)
        
    if 4/5 <= i <= 2:
        result_biotic.append(5)
        
      
bioticg = sum(result_biotic) / 9

#===================================== radar chart by turn=======================
# setting radar chart = importing a list from each parameter and put them into values list
#
for u in range(len(r3)):
    parameter = ['Biomass', 'Temperature', 'Biotic Landuse', 'Co2 Emission']
    values = [result_biomass[u] , tem_list[u], result_biotic[u], co2_list[u] ]
    N = len(parameter)
# generating values for the chart for each turn
    
#dividing circle with the number of parameter    
    x_as = [n / float(N) * 2 * pi for n in range(N)]

# Because our chart will be circular we need to append a copy of the first 
# value of each list at the end of each list with data
    values += values[:1]
    x_as += x_as[:1]


# Set color of axes
    plt.rc('axes', linewidth=0.5, edgecolor="#888888")


# Create polar plot
    ax = plt.subplot(111, polar=True)


# Set clockwise rotation. That is:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)


# Set position of y-labels
    ax.set_rlabel_position(45)


# Set color and linestyle of grid
    ax.xaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)
    ax.yaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)


# Set number of radial axes and remove labels
    plt.xticks(x_as[:-1], [])

# Set yticks
    plt.yticks([1, 2, 3, 4, 5], ["Bad", "Mediocre", "Average", "Good", "Great"])


# Plot data
    ax.plot(x_as, values, linewidth=0, linestyle='solid', zorder=3)

# Fill area
    ax.fill(x_as, values, 'b', alpha=0.3)


# Set axes limits
    plt.ylim(0, 5)


# Draw ytick labels to make sure they fit properly
    for i in range(N):
        angle_rad = i / float(N) * 2 * pi

        if angle_rad == 0:
            ha, distance_ax = "center", 1
        elif 0 < angle_rad < pi:
            ha, distance_ax = "left", 1
        elif angle_rad == pi:
            ha, distance_ax = "center", 1
        else:
            ha, distance_ax = "right", 1

        ax.text(angle_rad, 5 + distance_ax, parameter[i], size=10, horizontalalignment=ha, verticalalignment="center")


# Show polar plot
    t = u+1
    print ''
    print ''
    print ('This is a chart for %dth turn' %t)
    print ('You acheived %d score in this turn' %(sum(values)*5))
    plt.show()
    
    


# Bar graph--------------------------------------------------------------
print ''
print ''
print '##############This is Overall score and statistic####################3'

turns = [1,2,3,4,5,6,7,8,9]
plt.plot(turns, result_biotic, color='yellow')
plt.plot(turns, co2_list, color='blue')
plt.plot(turns, result_biomass, color='g')
plt.plot(turns, tem_list, color='r')
plt.xlabel('Turns')
plt.ylabel('Grade')
plt.title('Overall Statistic')

#legend-----------------------------------------------------------
red_patch = mpatches.Patch(color='red', label='Temperature')
yellow_patch = mpatches.Patch(color='yellow', label='Biotic')
green_patch = mpatches.Patch(color='green', label='Biomass')
blue_patch = mpatches.Patch(color='blue', label='Co2')


plt.legend(handles=[red_patch, yellow_patch, green_patch, blue_patch], frameon = False)
plt.show()









#Radar Chart for overall=================================================================

parameter = ['Biomass', 'Temperature', 'Biotic Landuse', 'Co2 Emission']
values_overall = [bg , sum(tem_list)/9, bioticg, sum(co2_list)/9 ]

N = len(parameter)

x_as = [n / float(N) * 2 * pi for n in range(N)]

# Because our chart will be circular we need to append a copy of the first 
# value of each list at the end of each list with data
values_overall += values_overall[:1]
x_as += x_as[:1]


# Set color of axes
plt.rc('axes', linewidth=0.5, edgecolor="#888888")


# Create polar plot
ax = plt.subplot(111, polar=True)


# Set clockwise rotation. That is:
ax.set_theta_offset(pi / 2)
ax.set_theta_direction(-1)


# Set position of y-labels
ax.set_rlabel_position(45)


# Set color and linestyle of grid
ax.xaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)
ax.yaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)


# Set number of radial axes and remove labels
plt.xticks(x_as[:-1], [])

# Set yticks
plt.yticks([1, 2, 3, 4, 5], ["Bad", "Mediocre", "Average", "Good", "Great"])


# Plot data
ax.plot(x_as, values_overall, linewidth=0, linestyle='solid', zorder=3)

# Fill area
ax.fill(x_as, values_overall, 'b', alpha=0.3)


# Set axes limits
plt.ylim(0, 5)


# Draw ytick labels to make sure they fit properly
for i in range(N):
    angle_rad = i / float(N) * 2 * pi

    if angle_rad == 0:
        ha, distance_ax = "center", 1
    elif 0 < angle_rad < pi:
        ha, distance_ax = "left", 1
    elif angle_rad == pi:
        ha, distance_ax = "center", 1
    else:
        ha, distance_ax = "right", 1

    ax.text(angle_rad, 5 + distance_ax, parameter[i], size=10, horizontalalignment=ha, verticalalignment="center")


# Show polar plot


plt.show()
print ('###############You acheived %d score in this turn###################' %(sum(values_overall)*5))
  
            
            

    



