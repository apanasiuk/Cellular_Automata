# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 11:07:48 2018

@author: Nils
"""

# import general database
import csv
import math

GD_file = open('General_Database.csv', 'r')
  #reader = csv.reader(GD)
  #GD_list = list(reader)

GD = csv.reader (GD_file)

GH_tr_nr = []
LU_abiotic = []
LU_agri = []
LU_green = []
LU_urban = []
LU_water = []
population = []
rain = []
temp = []

for row in GD:
    GH_tr_nr.append(row[1])
    LU_abiotic.append(row[2])
    LU_agri.append(row[3])
    LU_green.append(row[4])
    LU_urban.append(row[5])
    LU_water.append(row[6])
    population.append(row[8])
    
GD_file.close()

del LU_abiotic [0]
del LU_agri [0]
del LU_green [0]
del LU_urban [0]
del LU_water [0]
del population [0]


turns = 0

def pop_by_year (year, triangle_number):    
    # 0-POPULATION GROWTH (imports tr_pop and transforms it to new_pop which increases according to the turns year)
        # in 2000 the growth rate was 1,33% with a total population of 6.1 billion people
        # The UN-medium-growth prediction is considered the most realistic and estimates a peak of 9.22 billion people in 2075
    if float(population [triangle_number]) == 0:
        loc_pop = 0
        return loc_pop
    else :   
        K = 9220000000
        P0 = 6100000000
        # i had to tweak the maximum growth rate to a higher number in order to match the UN-medium-growth predictions
        r = 0.05
        t = year - 2000   
        part_a = K*P0*math.exp(r*t)
        part_b = K + P0*(math.exp(r*t)-1)
        tot_pop = (part_a /part_b)
        loc_pop_fact = (float(population [triangle_number])/7632763834) # 
        loc_pop = loc_pop_fact*tot_pop
        return loc_pop


while turns < 100:

    turns += 10
    year = 2008 + turns

# POPULATION GROWTH    
    # iterates over 320 triangles and calculates the current population
    for triangle_number in range (320):
        loc_pop = pop_by_year (year, triangle_number)


        print 'triangle %d' %triangle_number
        print 'year %d' %year
        print 'local population = %d' %loc_pop
else :
    print 'end of the game'