import Rhino.Geometry as rg
import math as math 
import Grasshopper as gh

# Rules
#Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
#Any live cell with two or three live neighbours lives on to the next generation.
#Any live cell with more than three live neighbours dies, as if by overpopulation.
#Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.


def controller (FF,FI,t):# faces adjacancy, face status and iteration time
    n=len(FI)
    m=len(FF)
    LOD=[] # live or dead at "t=0" time. The first step is based on user's input
    for i in range(0,m):
        LOD.Add(0)
        for j in range (0,n):
            if i==FI[j]:
                LOD[i]=1
    if t==0:
        LODT=LOD
    else:
        step=1
        while(step<=t):
            LODT=[] #Creating a new list "Live" or "dead" at the time "t+1"
            for i in range (0,m):
                LODT.Add(0) # initializing values>> 0=dead for all cells
                checklist=FF[i]  # collecting the index of all faces neighbor to face(i)
                checklistCount=[]# initializing a check list to determine which neighbor is alive
                for k in range(0,len(checklist)):# for every neighbor in checklist
                    if LOD[checklist[k]]==1:# if that neighbor is alive
                         checklistCount.Add(1)#  add 1 to the check list 
                    if LOD[checklist[k]]==0:#if that neighbor is dead
                         checklistCount.Add(0)#  add 0 to the check list 
                if(sum(checklistCount)==2 and LOD[i]==1):#Any live cell with two or three live neighbours lives on to the next generation.
                    LODT[i]=1
                if(sum(checklistCount)==3 and LOD[i]==1):#Any live cell with two or three live neighbours lives on to the next generation.
                    LODT[i]=1
                if(sum(checklistCount)==3 and LOD[i]==0):#Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                    LODT[i]=1
            LOD=LODT
            step=step+1
    return LODT


LODC=controller(FF,FI,t)
