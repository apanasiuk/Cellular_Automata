#! /usr/bin/env python

# a simple dictionary
person1 = {'name' : "Winy", 'surname' : "Maas", 'age' : 50}
print person1['name'] 

# add a new key and value
person1['occuption'] = "architect"
print person1 
print "\n"

# a new person
person2 = person1
person2['name'] = "Jacob"
person2['surname'] = "van Rijs"
print person1
print person2
print "\n"
# WTF

person3 = dict(person2)
person3['name'] = "Natahlie"
person3['surname'] = "de Vries"
print person3

# a simple database?
allPersons = [person1, person2, person3]
